[![pipeline status](https://gitlab.com/wpdesk/ups-shipping-service/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/ups-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/ups-shipping-service/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/ups-shipping-service/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/ups-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/ups-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/ups-shipping-service/downloads)](https://packagist.org/packages/wpdesk/ups-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/ups-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/ups-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/ups-shipping-service/license)](https://packagist.org/packages/wpdesk/ups-shipping-service) 

# UPS Shipping Service

A library which implements https://gitlab.com/wpdesk/predators/library/abstract-shipping and uses the UPS API connection to obtain the UPS shipping live rates in return. 

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/ups-shipping-service
```

## Example usage

### Obtain the UPS rates for shipment 

```php
<?php
/*
 * @see https://gitlab.com/wpdesk/predators/library/abstract-shipping#creating-shipment-from-woocommerce-cart-contents
 */
$shipment = create_shipment(); 

$service_settings = new \WPDesk\AbstractShipping\Settings\SettingsValuesAsArray([
    'user_id' => 'Your UPS user ID',
    'password' => 'Your UPS password',
    'access_key' => 'Your UPS access key',
    'account_number' => 'Your UPS acount number',
]);

$rates = $service->rate_shipment($service_settings, $shipment);
print_r( $rates );
```
