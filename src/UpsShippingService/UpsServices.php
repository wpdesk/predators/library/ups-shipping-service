<?php
/**
 * UPS implementation: UpsServices class.
 *
 * @package WPDesk\UpsShippingService
 */

namespace WPDesk\UpsShippingService;

use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\WooCommerceShipping\ShopSettings;

/**
 * A class that defines UPS services.
 *
 * @package WPDesk\UpsShippingService
 */
class UpsServices {

    const SUREPOST_LESS_THAN_1_LB = '92';
    const SUREPOST_1_LB_OR_GREATER = '93';

    /**
	 * EU countries.
	 *
	 * @var array
	 */
	private static $eu_countries = [];

	/**
	 * Services.
	 *
	 * @var array
	 */
	private static $services = null;

	/**
	 * Get services.
	 *
	 * @return array
	 */
	public static function get_services() {
		if ( empty( self::$services ) ) {
			self::$services = [
				'all'   => [
					'96' => __( 'UPS Worldwide Express Freight', 'ups-shipping-service' ),
					'71' => __( 'UPS Worldwide Express Freight Midday', 'ups-shipping-service' ),
                ],
				'other' => [
					'07' => __( 'UPS Express', 'ups-shipping-service' ),
					'11' => __( 'UPS Standard', 'ups-shipping-service' ),
					'08' => __( 'UPS Worldwide Expedited', 'ups-shipping-service' ),
					'54' => __( 'UPS Worldwide Express Plus', 'ups-shipping-service' ),
					'65' => __( 'UPS Worldwide Saver', 'ups-shipping-service' ),
                ],
				'PR'    => [ // Puerto Rico.
					'02' => __( 'UPS 2nd Day Air', 'ups-shipping-service' ),
					'03' => __( 'UPS Ground', 'ups-shipping-service' ),
					'01' => __( 'UPS Next Day Air', 'ups-shipping-service' ),
					'14' => __( 'UPS Next Day Air Early', 'ups-shipping-service' ),
					'08' => __( 'UPS Worldwide Expedited', 'ups-shipping-service' ),
					'07' => __( 'UPS Worldwide Express', 'ups-shipping-service' ),
					'54' => __( 'UPS Worldwide Express Plus', 'ups-shipping-service' ),
					'65' => __( 'UPS Worldwide Saver', 'ups-shipping-service' ),
                ],
				'PL'    => [ // Poland.
					'70' => __( 'UPS Access Point Economy', 'ups-shipping-service' ),
					'83' => __( 'UPS Today Dedicated Courrier', 'ups-shipping-service' ),
					'85' => __( 'UPS Today Express', 'ups-shipping-service' ),
					'86' => __( 'UPS Today Express Saver', 'ups-shipping-service' ),
					'82' => __( 'UPS Today Standard', 'ups-shipping-service' ),
					'08' => __( 'UPS Expedited', 'ups-shipping-service' ),
					'07' => __( 'UPS Express', 'ups-shipping-service' ),
					'54' => __( 'UPS Express Plus', 'ups-shipping-service' ),
					'65' => __( 'UPS Express Saver', 'ups-shipping-service' ),
					'11' => __( 'UPS Standard', 'ups-shipping-service' ),
                ],
				'MX'    => [ // Mexico.
					'70' => __( 'UPS Access Point Economy', 'ups-shipping-service' ),
					'08' => __( 'UPS Expedited', 'ups-shipping-service' ),
					'07' => __( 'UPS Express', 'ups-shipping-service' ),
					'11' => __( 'UPS Standard', 'ups-shipping-service' ),
					'54' => __( 'UPS Worldwide Express Plus', 'ups-shipping-service' ),
					'65' => __( 'UPS Worldwide Saver', 'ups-shipping-service' ),
                ],
				'EU'    => [ // European Union.
					'70' => __( 'UPS Access Point Economy', 'ups-shipping-service' ),
					'08' => __( 'UPS Expedited', 'ups-shipping-service' ),
					'07' => __( 'UPS Express', 'ups-shipping-service' ),
					'11' => __( 'UPS Standard', 'ups-shipping-service' ),
					'54' => __( 'UPS Worldwide Express Plus', 'ups-shipping-service' ),
					'65' => __( 'UPS Worldwide Saver', 'ups-shipping-service' ),
                ],
				'CA'    => [ // Canada.
					'02' => __( 'UPS Expedited', 'ups-shipping-service' ),
					'13' => __( 'UPS Express Saver', 'ups-shipping-service' ),
					'12' => __( 'UPS 3 Day Select', 'ups-shipping-service' ),
					'70' => __( 'UPS Access Point Economy', 'ups-shipping-service' ),
					'01' => __( 'UPS Express', 'ups-shipping-service' ),
					'14' => __( 'UPS Express Early', 'ups-shipping-service' ),
					'65' => __( 'UPS Express Saver', 'ups-shipping-service' ),
					'11' => __( 'UPS Standard', 'ups-shipping-service' ),
					'08' => __( 'UPS Worldwide Expedited', 'ups-shipping-service' ),
					'07' => __( 'UPS Worldwide Express', 'ups-shipping-service' ),
					'54' => __( 'UPS Worldwide Express Plus', 'ups-shipping-service' ),
                ],
				'US'    => [ // USA.
					'11' => __( 'UPS Standard', 'ups-shipping-service' ),
					'07' => __( 'UPS Worldwide Express', 'ups-shipping-service' ),
					'08' => __( 'UPS Worldwide Expedited', 'ups-shipping-service' ),
					'54' => __( 'UPS Worldwide Express Plus', 'ups-shipping-service' ),
					'65' => __( 'UPS Worldwide Saver', 'ups-shipping-service' ),
					'02' => __( 'UPS 2nd Day Air', 'ups-shipping-service' ),
					'59' => __( 'UPS 2nd Day Air A.M.', 'ups-shipping-service' ),
					'12' => __( 'UPS 3 Day Select', 'ups-shipping-service' ),
					'03' => __( 'UPS Ground', 'ups-shipping-service' ),
					'01' => __( 'UPS Next Day Air', 'ups-shipping-service' ),
					'14' => __( 'UPS Next Day Air Early', 'ups-shipping-service' ),
					'13' => __( 'UPS Next Day Air Saver', 'ups-shipping-service' ),
                ],
            ];
		}

		return self::$services;
	}

    /**
     * @return array
     */
	public function get_surepost_services() {
	    return [
            self::SUREPOST_LESS_THAN_1_LB  => __( 'SurePost Less than 1 lb', 'ups-shipping-service' ),
            self::SUREPOST_1_LB_OR_GREATER => __( 'SurePost 1 lb or Greater', 'ups-shipping-service' ),
            '94'                           => __( 'SurePost BPM', 'ups-shipping-service' ),
            '95'                           => __( 'SurePost Media Mail', 'ups-shipping-service' ),
        ];
    }

    /**
     * @return array
     */
    public function get_surepost_same_services() {
	    return [
	        [ self::SUREPOST_LESS_THAN_1_LB, self::SUREPOST_1_LB_OR_GREATER ]
        ];
    }

	/**
	 * Set EU countries.
	 *
	 * @param array $eu_countries .
	 */
	public static function set_eu_countries( array $eu_countries ) {
		self::$eu_countries = $eu_countries;
	}

	/**
	 * Get services for country.
	 *
	 * @param string $country_code .
	 *
	 * @return array
	 */
	public static function get_services_for_country( $country_code ) {
		$services             = self::get_services();
		$services_for_country = [];
		if ( isset( $services[ $country_code ] ) ) {
			$services_for_country = $services[ $country_code ];
		}
		if ( 'PL' !== $country_code && in_array( $country_code, self::$eu_countries, true ) ) {
			$services_for_country = $services['EU'];
		}
		if ( 0 === count( $services_for_country ) ) {
			$services_for_country = $services['other'];
		}
		foreach ( $services['all'] as $key => $value ) {
			$services_for_country[ $key ] = $value;
		}
		return $services_for_country;
	}

}

