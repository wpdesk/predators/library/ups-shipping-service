<?php
/**
 * UPS API: Sender interface.
 *
 * @package WPDesk\UpsShippingService\UpsApi
 */

namespace WPDesk\UpsShippingService\UpsApi;

use Ups\Entity\RateRequest;
use Ups\Entity\RateResponse;

/**
 * Sender class interface.
 */
interface Sender {

	/**
	 * Send request.
	 *
	 * @param RateRequest $request $request Request.
	 *
	 * @return RateResponse
	 */
	public function send( RateRequest $request );

}
