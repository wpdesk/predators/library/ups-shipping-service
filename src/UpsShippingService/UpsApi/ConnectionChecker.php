<?php
/**
 * Connection checker.
 *
 * @package WPDesk\UpsShippingService\UpsApi
 */

namespace WPDesk\UpsShippingService\UpsApi;

use Psr\Log\LoggerInterface;
use Ups\Entity\Address;
use Ups\Entity\Package;
use Ups\Entity\PackageWeight;
use Ups\Entity\RateRequest;
use Ups\Entity\UnitOfMeasurement;
use Ups\Rate;
use Ups\SimpleAddressValidation;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Settings\SettingsValuesAsArray;
use WPDesk\UpsShippingService\UpsSettingsDefinition;
use WPDesk\UpsShippingService\UpsShippingService;

/**
 * Can check connection.
 */
class ConnectionChecker {

	/**
	 * Shipping service.
	 *
	 * @var UpsShippingService
	 */
	private $shipping_service;

	/**
	 * Settings.
	 *
	 * @var SettingsValuesAsArray
	 */
	private $settings;

	/**
	 * Logger.
	 *
	 * @var LoggerInterface
	 */
	private $logger;

    /**
     * @var Sender
     */
    private $sender;

    /**
	 * ConnectionChecker constructor.
	 *
	 * @param UpsShippingService $shipping_service .
	 * @param SettingsValues     $settings .
	 * @param LoggerInterface    $logger .
	 */
	public function __construct( UpsShippingService $shipping_service, Sender $sender, SettingsValues $settings, $logger ) {
		$this->shipping_service = $shipping_service;
		$this->settings         = $settings;
		$this->logger           = $logger;
        $this->sender           = $sender;
	}

	/**
	 * Pings API.
	 *
	 * @throws \Exception .
	 */
	public function check_connection() {
		$address = new Address();

		$address->setStateProvinceCode( 'NY' );
		$address->setCity( 'New York' );
		$address->setCountryCode( 'US' );
		$address->setPostalCode( '10000' );

		$request = new RateRequest();

		$package = new Package();
		$weight = new PackageWeight();
		$weight->setWeight(1 );
        $weight->getUnitOfMeasurement()->setCode( UnitOfMeasurement::UOM_LBS );
		$package->setPackageWeight( $weight );

		$request->getShipment()->addPackage( $package );
		$request->getShipment()->getShipper()->setAddress( $address );
		$request->getShipment()->getShipTo()->setAddress( $address );

        $this->sender->send( $request );

	}

}
