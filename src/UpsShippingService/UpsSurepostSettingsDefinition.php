<?php

namespace WPDesk\UpsShippingService;

use Ups\Entity\PickupType;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\WooCommerceShipping\FreeShipping\FreeShippingFields;
use WPDesk\WooCommerceShipping\ShippingMethod\RateMethod\Fallback\FallbackRateMethod;
use WPDesk\WooCommerceShipping\ShopSettings;

/**
 * A class that defines the basic settings for the shipping method.
 *
 * @package WPDesk\UpsShippingService
 */
class UpsSurepostSettingsDefinition extends SettingsDefinition {

	const METHOD_SETTINGS_TITLE  = 'method_settings_title';
	const TITLE                  = 'title';
	const FALLBACK               = 'fallback';
    const SUREPOST_SERVICES      = 'surepost_services';
	const RATE_ADJUSTMENTS_TITLE = 'rate_adjustments_title';
	const NEGOTIATED_RATES       = 'negotiated_rates';
	const INSURANCE              = 'insurance';
	const PICKUP_TYPE            = 'pickup_type';
	const FREE_SHIPPING          = 'free_shipping';

    const NOT_SET = 'not_set';

    const DEFAULT_PICKUP_TYPE = self::NOT_SET;

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return true;
	}

	/**
	 * Initialise Settings Form Fields.
	 */
	public function get_form_fields() {

		$ups_services = new UpsServices();

		$instance_fields = array(
			self::METHOD_SETTINGS_TITLE  => array(
				'title'       => __( 'Method Settings', 'ups-shipping-service' ),
				'description' => __( 'Set how UPS services are displayed.', 'ups-shipping-service' ),
				'type'        => 'title',
			),
			self::TITLE                  => array(
				'title'       => __( 'Method Title', 'ups-shipping-service' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout when fallback is used.',
					'ups-shipping-service'
				),
				'default'     => __( 'UPS SurePost Live Rates', 'ups-shipping-service' ),
				'desc_tip'    => true,
			),
			self::FALLBACK               => array(
				'type'    => FallbackRateMethod::FIELD_TYPE_FALLBACK,
				'default' => '',
			),
			self::FREE_SHIPPING               => array(
				'title'   => __( 'Free Shipping', 'ups-shipping-service' ),
				'type'    => FreeShippingFields::FIELD_TYPE_FREE_SHIPPING,
				'default' => '',
			),
            self::SUREPOST_SERVICES               => array(
                'title'   => __( 'Services', 'ups-shipping-service' ),
                'type'    => 'services',
                'options' => $ups_services->get_surepost_services(),
                'default' => '',
            ),
			self::RATE_ADJUSTMENTS_TITLE => array(
				'title'       => __( 'Rates Adjustments', 'ups-shipping-service' ),
				'description' => sprintf( __( 'Adjust these settings to get more accurate rates. Read %swhat affects the UPS rates in UPS WooCommerce plugin →%s', 'ups-shipping-service' ),
					sprintf( '<a href="%s" target="_blank">', __( 'https://wpde.sk/ups-free-rates-eng/', 'ups-shipping-service' ) ),
					'</a>'
				),
				'type'        => 'title',
			),
			self::NEGOTIATED_RATES       => array(
				'title'       => __( 'Negotiated Rates', 'ups-shipping-service' ),
				'label'       => __( 'Enable negotiated rates', 'ups-shipping-service' ),
				'type'        => 'checkbox',
				'description' => __( 'Enable this option only if your shipping account has negotiated rates available.',
					'ups-shipping-service'
				),
				'desc_tip'    => true,
				'default'     => 'no',
			),
			self::PICKUP_TYPE            => array(
				'title'       => __( 'Pickup Type', 'ups-shipping-service' ),
				'type'        => 'select',
				'description' => __( '\'Pickup Type\' may affect the live rates. In most cases selecting the \'Customer Counter\' or \'One Time Pickup\' grants the most accurate rates. If the \'Not set\' option has been chosen, the \'Pickup Type\' value will not be sent in the UPS API request.', 'ups-shipping-service' ),
                'desc_tip'    => true,
				'default'     => self::DEFAULT_PICKUP_TYPE,
                'options'     => array(
                    self::NOT_SET         => __( 'Not set', 'ups-shipping-service' ),
                    PickupType::PKT_DAILY => __( 'Daily Pickup', 'ups-shipping-service' ),
                ),
			),
		);

		return $instance_fields;
	}

}
