## [3.3.0] - 2024-11-06
### Added
- client credentials flow in REST API

## [3.2.2] - 2024-08-20
### Fixed
- locator response for single access point

## [3.2.1] - 2024-06-04
### Fixed 
- code refactor

## [3.2.0] - 2024-06-04
### Added
- currency codes translator

## [3.1.2] - 2024-04-10
### Fixed
- Invalid argument supplied for foreach 

## [3.1.1] - 2023-06-12
### Fixed
- API names

## [3.1.0] - 2023-06-12
### Changed
- REST API client moved to separate library

## [3.0.3] - 2023-06-05
### Fixed
- API URL

## [3.0.2] - 2023-06-02
### Added
- Additional info to rate request

## [3.0.1] - 2023-06-02
### Fixed
- Default API type

## [3.0.0] - 2023-05-30
### Added
- UPS REST API

## [2.13.1] - 2023-03-01
### Changed
- upgraded UPS API
- removed service code from API request

## [2.12.0] - 2022-12-29
### Changed
- used custom origin from shipping library

## [2.11.1] - 2022-08-30
### Fixed
- de_DE translations

## [2.11.0] - 2022-08-16
### Added
- de_DE translations

## [2.10.2] - 2022-04-12
### Changed
- Docs URL

## [2.10.1] - 2022-01-17
### Fixed
- connection checker - call rating api instead address verification

## [2.9.4] - 2021-11-10
### Fixed
- Polish translations

## [2.9.3] - 2021-11-08
### Fixed
- SurePost - weight unit for service 92 (SurePost Less than 1 lb)

## [2.9.1] - 2021-11-05
### Fixed
- SurePost - settings

## [2.9.0] - 2021-11-03
### Changed
- SurePost - settings for separate shipping method

## [2.8.2] - 2021-11-02
### Fixed
- SurePost - SurePost Less than 1 lb weight converter

## [2.8.1] - 2021-11-02
### Fixed
- SurePost - SurePost Less than 1 lb accepts weight in OZS

## [2.8.0] - 2021-10-28
### Changed
- SurePost - now works for sure

## [2.7.4] - 2021-10-18
### Changed
- Typos in texts

## [2.7.3] - 2021-10-04
### Changed
- Default shipping method title

## [2.7.2] - 2021-10-01
### Added
- Fixing potential error

## [2.7.1] - 2021-10-01
### Added
- Changed method name and typo for option

## [2.7.0] - 2021-07-26
### Added
- SurePost services

## [2.6.4] - 2021-07-19
### Changed
- texts

## [2.6.3] - 2021-07-15
### Changed
- pickup type is not required
- default pickup type option: Not set

## [2.6.2] - 2021-03-03
### Changed
- pickup type description

## [2.6.1] - 2021-03-02
### Changed
- default pickup type

## [2.6.0] - 2021-03-02
### Added
- moved pickup type from PRO version

## [2.5.1] - 2021-01-12
### Changed
- Position of Free Shipping field

## [2.5.0] - 2021-01-11
### Added
- Free Shipping support field

## [2.4.3] - 2020-10-21
### Fixed
- Catch exception during prepare country state options

## [2.4.2] - 2020-07-28
### Fixed
- locked guzzlehttp/guzzle on version 6.5.2 as newer version uses php_intl library

## [2.4.1] - 2020-07-27
### Fixed
- Search for access point by city
### Changed
- Updated "guzzlehttp/guzzle" package
### Added
- URLs to article about Pickup Type in PRO version

## [2.4.0] - 2020-07-22
### Added
- Pickup Type field in settings

## [2.3.10] - 2020-04-20
### Fixed
- locked guzzlehttp/guzzle on version 6.5.2 as newer version uses php_intl library

## [2.3.9] - 2020-02-19
### Fixed
- API Status field

## [2.3.8] - 2020-02-04
### Fixed
- negotiated rates

## [2.3.7] - 2020-01-27
### Added
- minimal package weight: 0.1

## [2.3.6] - 2020-01-27
### Changed
- create_sender method

## [2.3.5] - 2020-01-23
### Fixed
- added missing package dimensions

## [2.3.4] - 2020-01-16
### Fixed
- translations

## [2.3.3] - 2020-01-13
### Fixed
- text domain in translations

## [2.3.2] - 2020-01-13
### Added
- rate currency verification

## [2.3.1] - 2020-01-12
### Fixed
- package weight without packer

## [2.3.0] - 2020-01-11
### Added
- multiple packages - packer support

## [2.2.0] - 2020-01-08
### Added
- create_shipment_rating_implementation method

## [2.1.0] - 2020-01-08
### Changed
- create_reply_interpretation method parameters

## [2.0.3] - 2020-01-08
### Changed
- UPS library version

## [2.0.2] - 2019-12-19
### Changed
- rate request builder

## [2.0.1] - 2019-12-13
### Added
- test mode

## [2.0.0] - 2019-12-13
### Added
- abstract shipping v 2.0.0

## [1.0.4] - 2019-12-13
### Added
- access points only rates

## [1.0.3] - 2019-12-11
### Added
- service description

## [1.0.2] - 2019-12-10
### Added
- connection checker

## [1.0.1] - 2019-12-09
### Added
- settings defaults

## [1.0.0] - 2019-11-15
### Added
- initial version
